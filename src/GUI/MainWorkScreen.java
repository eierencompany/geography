package GUI;

import Entity.City;
import Entity.Country;
import SessionCreate.WorkGeography;
import Classes.CityWorker;
import java.awt.Color;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.swing.table.DefaultTableModel;

public class MainWorkScreen extends javax.swing.JFrame {
    
    public EntityManager em;
    WorkGeography connect = new WorkGeography();
    
    public MainWorkScreen() {
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/image/logo.png")));
        initComponents();
        getContentPane().setBackground(Color.WHITE);
        GenerateComboBoxList();
    }
    
    // <editor-fold defaultstate="collapsed" desc="������ ��� ���������� HashMap">    
    private void CreateMapCityCode(Map<String, String> CityCode){
        connect.Session();
        for (City CityList: new City().getCityList(connect.em)){
            Country CountryList = CityList.getCountryCode();
            CityCode.put(CityList.getName(), CountryList.getCode());
        }
    }
    private void CreateMapContinentCode(Map<String, List<String>> CountryCode){
        connect.Session();
        for (Country CountryList: new Country().getCountryList(connect.em)){
            String Continent = CountryList.getContinent();
            List<Country> CodeForList = CountryList.getCountryCodeList(connect.em, Continent);
            List<String> CodeList = new ArrayList<>();
            for (int i = 0; i < CodeForList.size(); i++){
                String Code = String.valueOf(CodeForList.get(i));
                CodeList.add(Code);
            }
            CountryCode.put(Continent, CodeList);
        }
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="��������� jComboBox">
        private void GenerateComboBoxList(){ 
            connect.Session();
            for (String country: new Country().getCountryListForComboBox(connect.em)){
                SearchCity.addItem(country);
            }
        }
    //</editor-fold>
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelForNameApp = new javax.swing.JPanel();
        NameApp = new javax.swing.JLabel();
        MainWorkPane = new javax.swing.JTabbedPane();
        WorkPanelFirst = new javax.swing.JPanel();
        LabelSearch = new javax.swing.JLabel();
        SearchCity = new javax.swing.JComboBox<>();
        ScrollPaneTableFirst = new javax.swing.JScrollPane();
        TableForFirstResult = new javax.swing.JTable();
        WorkPanelSecond = new javax.swing.JPanel();
        TextRun = new javax.swing.JLabel();
        Calculate = new javax.swing.JButton();
        ScrollPaneTableSecond = new javax.swing.JScrollPane();
        TableForSecondResult = new javax.swing.JTable();
        WorkPanelThird = new javax.swing.JPanel();
        TextSearchSecond = new javax.swing.JLabel();
        Search = new javax.swing.JButton();
        ScrollPaneTableThird = new javax.swing.JScrollPane();
        TableForThirdResult = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("�������������� ��������");
        setResizable(false);

        PanelForNameApp.setBackground(new java.awt.Color(51, 51, 51));

        NameApp.setFont(new java.awt.Font("Segoe UI Black", 0, 24)); // NOI18N
        NameApp.setForeground(new java.awt.Color(255, 255, 255));
        NameApp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        NameApp.setText("�������������� ��������");

        javax.swing.GroupLayout PanelForNameAppLayout = new javax.swing.GroupLayout(PanelForNameApp);
        PanelForNameApp.setLayout(PanelForNameAppLayout);
        PanelForNameAppLayout.setHorizontalGroup(
            PanelForNameAppLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelForNameAppLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(NameApp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        PanelForNameAppLayout.setVerticalGroup(
            PanelForNameAppLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelForNameAppLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(NameApp, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                .addContainerGap())
        );

        MainWorkPane.setTabPlacement(javax.swing.JTabbedPane.LEFT);

        LabelSearch.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        LabelSearch.setText("�����:");

        SearchCity.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                SearchCityItemStateChanged(evt);
            }
        });

        TableForFirstResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "���������", "�����"
            }
        ));
        ScrollPaneTableFirst.setViewportView(TableForFirstResult);

        javax.swing.GroupLayout WorkPanelFirstLayout = new javax.swing.GroupLayout(WorkPanelFirst);
        WorkPanelFirst.setLayout(WorkPanelFirstLayout);
        WorkPanelFirstLayout.setHorizontalGroup(
            WorkPanelFirstLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WorkPanelFirstLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WorkPanelFirstLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(ScrollPaneTableFirst, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(WorkPanelFirstLayout.createSequentialGroup()
                        .addComponent(LabelSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SearchCity, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        WorkPanelFirstLayout.setVerticalGroup(
            WorkPanelFirstLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WorkPanelFirstLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WorkPanelFirstLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LabelSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(SearchCity))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneTableFirst, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        MainWorkPane.addTab("<html> <center> ����� �������<br>�� ���������� </center> </html>", WorkPanelFirst);

        TextRun.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TextRun.setText("����������: ");

        Calculate.setText("����������");
        Calculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalculateActionPerformed(evt);
            }
        });

        TableForSecondResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "���������", "����������"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        ScrollPaneTableSecond.setViewportView(TableForSecondResult);

        javax.swing.GroupLayout WorkPanelSecondLayout = new javax.swing.GroupLayout(WorkPanelSecond);
        WorkPanelSecond.setLayout(WorkPanelSecondLayout);
        WorkPanelSecondLayout.setHorizontalGroup(
            WorkPanelSecondLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WorkPanelSecondLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WorkPanelSecondLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(ScrollPaneTableSecond, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(WorkPanelSecondLayout.createSequentialGroup()
                        .addComponent(TextRun)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Calculate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        WorkPanelSecondLayout.setVerticalGroup(
            WorkPanelSecondLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WorkPanelSecondLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WorkPanelSecondLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(WorkPanelSecondLayout.createSequentialGroup()
                        .addComponent(Calculate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1))
                    .addComponent(TextRun, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneTableSecond, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        MainWorkPane.addTab("<html>\n<center>\n����������<br>���-�� �������\n</center>\n</html>", WorkPanelSecond);

        TextSearchSecond.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        TextSearchSecond.setText("�����:");

        Search.setText("�����");
        Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchActionPerformed(evt);
            }
        });

        TableForThirdResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "���������", "����������"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        ScrollPaneTableThird.setViewportView(TableForThirdResult);

        javax.swing.GroupLayout WorkPanelThirdLayout = new javax.swing.GroupLayout(WorkPanelThird);
        WorkPanelThird.setLayout(WorkPanelThirdLayout);
        WorkPanelThirdLayout.setHorizontalGroup(
            WorkPanelThirdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WorkPanelThirdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WorkPanelThirdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(ScrollPaneTableThird, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(WorkPanelThirdLayout.createSequentialGroup()
                        .addComponent(TextSearchSecond, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        WorkPanelThirdLayout.setVerticalGroup(
            WorkPanelThirdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WorkPanelThirdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WorkPanelThirdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(WorkPanelThirdLayout.createSequentialGroup()
                        .addComponent(Search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1))
                    .addComponent(TextSearchSecond, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ScrollPaneTableThird, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        MainWorkPane.addTab("<html>\n<center>\n�����<br>���������� ����������\n</center>\n</html>", WorkPanelThird);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelForNameApp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(MainWorkPane, javax.swing.GroupLayout.PREFERRED_SIZE, 560, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelForNameApp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(MainWorkPane, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void SearchCityItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_SearchCityItemStateChanged
        connect.Session();
        
        CityWorker cw = new CityWorker(); //���������� ��� ������
        
        Map<String, String> CityCode = new HashMap<>(); //�����-��� ������
        Map<String, List<String>> ContinentCode = new HashMap<>(); //���������-��� ������
        CreateMapCityCode(CityCode);
        CreateMapContinentCode(ContinentCode);
        
        // <editor-fold defaultstate="collapsed" desc="������� ������� �� ������, ���� ����� ��� ����������">  
            DefaultTableModel dm = (DefaultTableModel)TableForFirstResult.getModel();
            while(dm.getRowCount() > 0)
            {
                dm.removeRow(0);
            }
        //</editor-fold>
        
        String SelectedCity = (String) SearchCity.getSelectedItem();
        
        if (SelectedCity.equals("Antarctica")){
            Vector<Object> oneRow = new Vector<>();
            oneRow.add(SelectedCity);
            oneRow.add("");
            dm.addRow(oneRow);
        } else {
            String Result = cw.findCitiesByContinent(CityCode, ContinentCode, SelectedCity);
        
            String[] ResultSplit = Result.split(":"); //��������� ��������� �� {���������},{�����;�����;�����;...}
            String[] CitySplit = ResultSplit[1].split("; "); //��������� ��������� �� {�����}, {�����}, {�����}...
            for (String CitySplit1 : CitySplit) {
                Vector<Object> oneRow = new Vector<Object>();
                if (dm.getRowCount() == 0){
                    oneRow.add(ResultSplit[0]);
                    oneRow.add(CitySplit1);
                } else {
                    oneRow.add("");
                    oneRow.add(CitySplit1);
                }
                dm.addRow(oneRow);
            }
        }
    }//GEN-LAST:event_SearchCityItemStateChanged

    private void CalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalculateActionPerformed
        connect.Session();
        
        CityWorker cw = new CityWorker(); //���������� ��� ������
        
        Map<String, List<String>> ContinentCode = new HashMap<>(); //���������-���
        CreateMapContinentCode(ContinentCode);
        
        // <editor-fold defaultstate="collapsed" desc="������� ������� �� ������, ���� ����� ��� ����������">  
            DefaultTableModel dm = (DefaultTableModel)TableForSecondResult.getModel();
            while(dm.getRowCount() > 0)
            {
                dm.removeRow(0);
            }
        //</editor-fold>
        
        Map<String, Integer> Result = cw.calculateCityCountsInContinents(ContinentCode);
        
        for (Map.Entry<String, Integer> ContinentCount: Result.entrySet()){
            Vector<Object> oneRow = new Vector<Object>();
            oneRow.add(ContinentCount.getKey());
            oneRow.add(ContinentCount.getValue());
            dm.addRow(oneRow);
        }
    }//GEN-LAST:event_CalculateActionPerformed

    private void SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchActionPerformed
        connect.Session();
        
        CityWorker cw = new CityWorker(); //���������� ��� ������
        
        Map<String, List<String>> ContinentCode = new HashMap<>(); //���������-��� ������
        CreateMapContinentCode(ContinentCode);
        
        // <editor-fold defaultstate="collapsed" desc="������� ������� �� ������, ���� ����� ��� ����������">  
            DefaultTableModel dm = (DefaultTableModel)TableForThirdResult.getModel();
            while(dm.getRowCount() > 0)
            {
                dm.removeRow(0);
            }
        //</editor-fold>
        
        String Result = cw.findContinentWithFewestCities(ContinentCode);
        
        String[] ResultSplit = Result.split(": "); //��������� ��������� �� {���������},{�����}
        
        Vector<Object> oneRow = new Vector<Object>();
        oneRow.add(ResultSplit[0]);
        oneRow.add(ResultSplit[1]);
        dm.addRow(oneRow);
    }//GEN-LAST:event_SearchActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWorkScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWorkScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWorkScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWorkScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainWorkScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Calculate;
    private javax.swing.JLabel LabelSearch;
    private javax.swing.JTabbedPane MainWorkPane;
    private javax.swing.JLabel NameApp;
    private javax.swing.JPanel PanelForNameApp;
    private javax.swing.JScrollPane ScrollPaneTableFirst;
    private javax.swing.JScrollPane ScrollPaneTableSecond;
    private javax.swing.JScrollPane ScrollPaneTableThird;
    private javax.swing.JButton Search;
    private javax.swing.JComboBox<String> SearchCity;
    private javax.swing.JTable TableForFirstResult;
    private javax.swing.JTable TableForSecondResult;
    private javax.swing.JTable TableForThirdResult;
    private javax.swing.JLabel TextRun;
    private javax.swing.JLabel TextSearchSecond;
    private javax.swing.JPanel WorkPanelFirst;
    private javax.swing.JPanel WorkPanelSecond;
    private javax.swing.JPanel WorkPanelThird;
    // End of variables declaration//GEN-END:variables
}
