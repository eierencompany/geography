package SessionCreate;

import GUI.MainWorkScreen;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class WorkGeography {
    
    public EntityManager em;
    
    public static void main(String[] args) {
        new MainWorkScreen().setVisible(true);
    }
    
    public void Session(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("WorkGeographyPU");
        em = emf.createEntityManager();
    }
}
